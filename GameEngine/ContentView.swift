//
//  ContentView.swift
//  GameEngine
//
//  Created by Indra Sumawi on 23/10/19.
//  Copyright © 2019 Indra Sumawi. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
